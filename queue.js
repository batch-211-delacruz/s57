let collection = [];
// Write the queue functions below. 


//Gives all the elements of the queue
function print(){
    return(collection);
}

//Adds an element/elements to the end of the queue
function enqueue(item){
    collection.push(item)
    return collection;
}

//Removes an element in front of the queue
function dequeue(){
 collection.shift(1)
 return collection;
}


//Shows the element at the front
function front(){
   var first = collection.filter(x => typeof x!==undefined).shift();
   return first;
}

//Shows the total number of elements
function size(){
    var total = collection.length;
    return total;
}

//Gives a Boolean value describing whether the queue is empty or not
function isEmpty(){
//     if(!collection[0]){
//      return false;
// }
// if(collection.length > 0){
//     return false;
// }else {
//     return true;
// }

    // var last = collection[collection.length - 1];
    // return last;
    // console.log(collection)
 if(collection.length == 0){
    return true;
 }else {
    return false
 }

}








// Export create queue functions below.
module.exports = {
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};